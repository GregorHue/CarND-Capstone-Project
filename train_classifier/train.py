import numpy as np
import tensorflow as tf
import cv2
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications

# dimensions of our images.
img_width, img_height = 150, 150

top_model_weights_path = 'bottleneck_fc_model.h5'
train_data_dir = 'data/train'
validation_data_dir = 'data/validation'
test_data_dir = 'data/test'
nb_train_samples = 1408
nb_validation_samples = 368
epochs = 50
batch_size = 16


def save_bottlebeck_features():
    datagen = ImageDataGenerator(rescale=1. / 255)

    # build the VGG16 network without top layers
    model = applications.VGG16(include_top=False, weights='imagenet')

    generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='sparse',
        shuffle=False)
    bottleneck_features_train = model.predict_generator(
        generator, nb_train_samples // batch_size)
    with open('bottleneck_features_train.npy', 'wb') as features_train_file:
        np.save(features_train_file, bottleneck_features_train)

    generator = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='sparse',
        shuffle=False)
    bottleneck_features_validation = model.predict_generator(
        generator, nb_validation_samples // batch_size)
    with open('bottleneck_features_validation.npy', 'wb') as features_validation_file:
        np.save(features_validation_file, bottleneck_features_validation)


def train_top_model():
    with open('bottleneck_features_train.npy', 'rb') as features_train_file:
        train_data = np.load(features_train_file)

    with open('bottleneck_features_validation.npy', 'rb') as features_validation_file:
        validation_data = np.load(features_validation_file)
    train_labels = np.array(
            [0] * 244 + [1] * 404 + [2] * 692 +[3] * 68)

    validation_labels = np.array(
            [0] * 63 + [1] * 115 + [2] * 170 +[3] * 20 )

    model = Sequential()
    print(train_data.shape[1:])
    model.add(Flatten(input_shape=train_data.shape[1:]))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4, activation='softmax'))

    model.compile(optimizer=optimizers.SGD(lr=0.01, momentum=0.0, decay=0.0, nesterov=False),
                  loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    model.fit(train_data, train_labels,
              epochs=epochs,
              batch_size=batch_size,
              validation_data=(validation_data, validation_labels))
    model.save_weights(top_model_weights_path)
    
def finetune():
    # build the VGG16 network without top layers
    #model = applications.VGG16(include_top=False, weights='imagenet')
    base_model = applications.VGG16(include_top=False, weights='imagenet', input_shape = (150, 150, 3))
    for layer in base_model.layers[:15]:
        layer.trainable = False
    print('Model loaded.')
    
    model = Sequential();
    model.add(base_model)
    
    # build a classifier model to put on top of the convolutional model
    top_model = Sequential()
    top_model.add(Flatten(input_shape=(4 ,4 , 512)))
    top_model.add(Dense(4096, activation='relu'))
    top_model.add(Dropout(0.5))
    top_model.add(Dense(4, activation='softmax'))
    
    # note that it is necessary to start with a fully-trained
    # classifier, including the top classifier,
    # in order to successfully do fine-tuning
    top_model.load_weights(top_model_weights_path)
    
    # add the model on top of the convolutional base
    model.add(top_model)                                                           
    
    
    # compile the model with a SGD/momentum optimizer
    # and a very slow learning rate.
    model.compile(loss='sparse_categorical_crossentropy',
                  optimizer=optimizers.SGD(lr=1e-4, momentum=0.9),
                  metrics=['accuracy'])
    batch_size = 16

    # prepare data augmentation configuration
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)
    
    test_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_height, img_width),
            batch_size=batch_size,
            class_mode='sparse')
    
    validation_generator = test_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_height, img_width),
            batch_size=batch_size,
            class_mode='sparse')
    
    # fine-tune the model
    epochs = 18;
    
    model.fit_generator(
            train_generator,
            steps_per_epoch=nb_train_samples // batch_size,
            epochs=epochs,
            validation_data=validation_generator,
            validation_steps=nb_validation_samples // batch_size)
    
    model.save('finetune.h5')
    
save_bottlebeck_features()
train_top_model()
finetune()

# save model to pb ====================
def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph

# parameter ==========================
wkdir = './'
pb_filename = 'finetune.pb'
from keras import backend as K

K.set_learning_phase(0)
# Load .h5 model
from keras.models import load_model
model = load_model('finetune.h5')
# save keras model as tf pb files ===============
from keras import backend as K

K.set_learning_phase(0)
tf.reset_default_graph()
frozen_graph = freeze_session(K.get_session(),
                              output_names=[out.op.name for out in model.outputs])
tf.train.write_graph(frozen_graph, wkdir, pb_filename, as_text=False)




# # load & inference the model ==================

from tensorflow.python.platform import gfile
with tf.Session() as sess:
    # load model from pb file
    with gfile.FastGFile(wkdir+'/'+pb_filename,'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        g_in = tf.import_graph_def(graph_def, name='my_model')
  
    # print all operation names 
    print('\n===== ouptut operation names =====\n')
    f = open('names', 'w')
    for op in sess.graph.get_operations():
       f.write(op.name + "\n")
    f.close()

    # inference by the model (op name must comes with :0 to specify the index of its output)
    tensor_output = sess.graph.get_tensor_by_name('my_model/sequential_2/dense_4/Softmax:0')
    tensor_input = sess.graph.get_tensor_by_name('my_model/input_2:0')
    # One test Image as sample input
    image = cv2.imread(test_data_dir + '/test/88.0_69.jpg')
    # Resizing the image to our desired size and
    # preprocessing will be done exactly as done during training
    image = cv2.resize(image, (img_width, img_height), cv2.INTER_LINEAR)
    image = np.array(image, dtype=np.uint8)
    image = image.astype('float64')
    image = np.multiply(image, 1.0/255.0) 
    image = image[np.newaxis,:]
    print(image.shape)
    print(type(image))
    predictions = sess.run(tensor_output, {tensor_input: image})
    print('\n===== output predicted results =====\n')
    print(predictions)

