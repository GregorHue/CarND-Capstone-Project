# Udacity Self-Driving Car Nanodegree Capstone Project
This is the final project of the Udacity Self-Driving Car Nanodegree. It is an individual submission without team building and was submitted by
> * Gregor Hueltenschmidt (Udacity Account: gregor.hueltenschmidt@web.de)
## Project Overview
Carla, the self-driving car of Udacity, has a ros based system architecture. The goal of this project is to develop an application that implements the core functionality of this architecture. This functionality comprises three vehicle subsystems.
* Perception
* Planning
* Control

Accordingly the ros architecture looks as follows:  

![](final-project-ros-graph-v2.png)
*<div style="text-align: center">The image was taken from lectures</div>*<br/>

After development the application shall be tested on a simulator and deployed on Carla.
## Starter Code Installation 
Starter code was provided by Udacity and can be found [here](https://github.com/udacity/CarND-Capstone).
These are the original instructions for setting up the starter code:

Please use **one** of the two installation options, either native **or** docker installation.

### Native Installation

* Be sure that your workstation is running Ubuntu 16.04 Xenial Xerus or Ubuntu 14.04 Trusty Tahir. [Ubuntu downloads can be found here](https://www.ubuntu.com/download/desktop).
* If using a Virtual Machine to install Ubuntu, use the following configuration as minimum:
  * 2 CPU
  * 2 GB system memory
  * 25 GB of free hard drive space

  The Udacity provided virtual machine has ROS and Dataspeed DBW already installed, so you can skip the next two steps if you are using this.

* Follow these instructions to install ROS
  * [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu) if you have Ubuntu 16.04.
  * [ROS Indigo](http://wiki.ros.org/indigo/Installation/Ubuntu) if you have Ubuntu 14.04.
* [Dataspeed DBW](https://bitbucket.org/DataspeedInc/dbw_mkz_ros)
  * Use this option to install the SDK on a workstation that already has ROS installed: [One Line SDK Install (binary)](https://bitbucket.org/DataspeedInc/dbw_mkz_ros/src/81e63fcc335d7b64139d7482017d6a97b405e250/ROS_SETUP.md?fileviewer=file-view-default)
* Download the [Udacity Simulator](https://github.com/udacity/CarND-Capstone/releases).

### Docker Installation
[Install Docker](https://docs.docker.com/engine/installation/)

Build the docker container
```bash
docker build . -t capstone
```

Run the docker file
```bash
docker run -p 4567:4567 -v $PWD:/capstone -v /tmp/log:/root/.ros/ --rm -it capstone
```

### Port Forwarding
To set up port forwarding, please refer to the "uWebSocketIO Starter Guide" found in the classroom (see Extended Kalman Filter Project lesson).

### Usage

1. Clone the project repository
```bash
git clone https://github.com/udacity/CarND-Capstone.git
```

2. Install python dependencies
```bash
cd CarND-Capstone
pip install -r requirements.txt
```
3. Make and run styx
```bash
cd ros
catkin_make
source devel/setup.sh
roslaunch launch/styx.launch
```
4. Run the simulator

### Real world testing
1. Download [training bag](https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/traffic_light_bag_file.zip) that was recorded on the Udacity self-driving car.
2. Unzip the file
```bash
unzip traffic_light_bag_file.zip
```
3. Play the bag file
```bash
rosbag play -l traffic_light_bag_file/traffic_light_training.bag
```
4. Launch your project in site mode
```bash
cd CarND-Capstone/ros
roslaunch launch/site.launch
```
5. Confirm that traffic light detection works on real life images

### Other library/driver information
Outside of `requirements.txt`, here is information on other driver/library versions used in the simulator and Carla:

Specific to these libraries, the simulator grader and Carla use the following:

|        | Simulator | Carla  |
| :-----------: |:-------------:| :-----:|
| Nvidia driver | 384.130 | 384.130 |
| CUDA | 8.0.61 | 8.0.61 |
| cuDNN | 6.0.21 | 6.0.21 |
| TensorRT | N/A | N/A |
| OpenCV | 3.2.0-dev | 2.4.8 |
| OpenMP | N/A | N/A |

We are working on a fix to line up the OpenCV versions between the two.

## My Implementation
### Ros Nodes Implementation
The implementation of the ros nodes heavily borrows from the walkthroughs in the lectures. Like in the CarND PID Control project a PID controller was implemented. The tuning of the parameters KP, KD, KI followed the description given in the PID project. For the implementation of the Traffic Light Detection Node a classifier was build and trained with keras. You can find the code under ```/train_classifier```. The neural network was trained with images collected from the simulator and were downloaded from [here](https://www.dropbox.com/s/vaniv8eqna89r20/alex-lechner-udacity-traffic-light-dataset.zip?dl=0). Thanks to the fellow student Alex Lechner for sharing this dataset. Training used transfer learning and was performed with tensorflow version 1.4.0 and keras version 2.0.8. Tensorflow version 1.4.0 is the minimum version because tensorflow prior to version 1.4.0 does not offer the opportunity to freeze a graph. This feature is necessary for converting the model obtained with keras in the h5 format into the tensorflow format pb. Fortunately the converted model is compatible with tensorflow version 1.3.0 that runs both on the simulator and on Carla. The code relies on [building-powerful-image-classification-models-using-very-little-data](https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html), [keras-tutorial-fine-tuning-using-pre-trained-models](https://www.learnopencv.com/keras-tutorial-fine-tuning-using-pre-trained-models/) and [keras_to_tensorflow](https://github.com/pipidog/keras_to_tensorflow). Training ended with an accuracy of 93 % in classifying traffic lights images.
### Run On Simulator
The code was tested on the simulator provided by udacity. The simulator offers the opportunity to switch off the camera. Thereby it was possible to test the planning and control functionality separately from the classification of the traffic lights colour. Unfortunately a car that stayed in lane without light detection left the road and crashed after switching on the camera. Various configurations were used:
* Capstone project workspace with gpu enabled: the car crashed after switching on the camera
* Simulator runs on a windows 10 machine with a i5 cpu processor, the code itself runs in the provided virtual machine on the same computer: the car crashed after switching on the camera
* Ubuntu 16.04 native installation on a machine with a NVIDIA GTX 1080 Titan X gpu. Both the simulator and the code were executed on this computer: The car stayed in lane after switching on the camera and stopped correctly at red traffic lights.<br/> Please note that the simulator did not run without issues on a computer with a german locale. It seems as if the simulator must run on a computer with an english locale probably due to the different decimal separator in Germany and in english speaking countries.
### Submission
The folder ```train_classifier``` comprises the code for training the model and the generated file of the layers names. The generated model ```finetune.pb``` could not be uploaded to GitLab due to space restrictions. Nevetheless it can be downloaded from [Dropbox](https://www.dropbox.com/s/v4m38bpzxe4nejm/finetune.pb?dl=0). The downloaded model has to be shifted into ```/ros/src/tl_detector/light_classification/model```.
### Video
Below you can watch a short video clip. It shows (in the simulator) a self-driving car that approaches a traffic light, stops at red and accelerates again at green.
![Capstone Project](capstone_project.mp4)
