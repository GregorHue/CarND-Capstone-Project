from styx_msgs.msg import TrafficLight
import tensorflow as tf
import numpy as np
import cv2
import rospy
import datetime
from tensorflow.python.platform import gfile

class TLClassifier(object):
    def __init__(self):
        #TODO load classifier

        PATH_TO_GRAPH = 'light_classification/model/finetune.pb'
    
	with tf.Session() as self.sess:
	    # load model from pb file
	    with gfile.FastGFile(PATH_TO_GRAPH,'rb') as f:
		graph_def = tf.GraphDef()
		graph_def.ParseFromString(f.read())
		self.sess.graph.as_default()
		g_in = tf.import_graph_def(graph_def, name='my_model')


    def get_classification(self, image):
        """Determines the color of the traffic light in the image
        Args:
            image (cv::Mat): image containing the traffic light
        Returns:
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)
        """
        # inference by the model (op name must comes with :0 to specify the index of its output)
        tensor_output = self.sess.graph.get_tensor_by_name('my_model/sequential_2/dense_4/Softmax:0')
        tensor_input = self.sess.graph.get_tensor_by_name('my_model/input_2:0')
        # Input
        # Resizing the image to our desired size and
        # preprocessing will be done exactly as done during training
	img_width, img_height = 150, 150
        image = cv2.resize(image, (img_width, img_height), cv2.INTER_LINEAR)
        image = np.array(image, dtype=np.uint8)
        image = image.astype('float64')
        image = np.multiply(image, 1.0/255.0) 
        image = image[np.newaxis,:]
   
        predictions = self.sess.run(tensor_output, {tensor_input: image})
        prediction = np.argmax(predictions)

        if prediction == 0:
            return TrafficLight.GREEN
        elif prediction == 1:
            return TrafficLight.RED
        elif prediction == 2:
	    return TrafficLight.UNKNOWN
        elif prediction == 3:
            return TrafficLight.YELLOW

        
